# Bootstrapping two-dimensional CFTs with Virasoro symmetry

## Basic features
This Python 3 code is written in the Jupyter Notebook format. 

Part of this code evolved from the GitHub repository [bootstrap-2d-Python](https://github.com/ribault/bootstrap-2d-Python).

BibTeX entry: @misc{bV, author = {{S. Ribault, et al}}, title = {Bootstrap\_Virasoro: Bootstrapping two-dimensional CFTs with Virasoro symmetry}, url = {https://gitlab.com/s.g.ribault/Bootstrap\_Virasoro}, year = {2019}, type = {code}, }

## Purpose
This code is 
for computing correlation functions in two-dimensional CFTs 
with Virasoro symmetry. We compute the spectrum and structure 
constants using analytic formulas, and the conformal blocks
using Zamolodchikov's reduction. Then we check crossing symmetry
numerically. This is done in CFTs like Liouville theory: non-rational,
at generic central charges. 



## Notebooks
* **CFT.ipynb** -- Basic CFT: central charge, conformal dimensions, fields. 
* **Auxiliary_classes.ipynb** -- A few useful scripts.
* **Blocks.ipynb** -- Conformal blocks.
* **Correlators.ipynb** -- Three- and four-point functions in Liouville theory and other CFTs, including some tests of crossing symmetry.
* **Tests.ipynb** -- Tests of crossing symmetry.
* **DMM_limit.ipynb** -- Figures for the article *"The non-rational limit of D-series minimal models"*.

## Technical notes

For 'Notebook1.ipynb' to be able to call 'Notebook2.ipynb', one should first generate 
the associated file 'Notebook2.py'. A script for generating such Python files 
can be called as 'bash manage.sh -m'. 

Before committing changes, one should clear the notebooks' output and metadata. 
A script for doing that can be called as 'bash manage.sh -c'. This script 
is based on the Python module 'nbstripout'.
